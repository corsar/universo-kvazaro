export default {
  namespaced: true,
  state() {
    return {
      auth: {
        user: null,
        loggedIn: null,
        konfirmita: null
      }
    }
  },
  getters: {
    user: (state) => {
      return state.auth.user
    },
    isLoggedIn: (state) => {

      return state.auth.loggedIn
    },
    konfirmita: (state) => {
      return state.auth.konfirmita
    }
  },
  mutations: {
    setUser: (state, payload) => state.auth.user = payload,
    setIsLoggedIn: (state, payload) => state.auth.loggedIn = payload,
    setKonfirmita: (state, payload) => state.auth.konfirmita = payload
  },
  actions: {}
}
