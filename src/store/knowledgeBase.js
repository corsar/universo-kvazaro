export default {
  namespaced: true,
  state() {
    return {
      knowledgeBase: [{
        uuid: 123321,
        parentUuid: 0,
        title: 'Верхний раздел',
        description: 'Описание верхнего раздела',
        dateAdd: '2020/10/25',
        links: ['https://www.youtube.com/watch?v=zZBiln_2FhM&t=20s'],
        submaterial: []
      }, {
        uuid: 123325,
        parentUuid: 0,
        title: 'Верхний раздел2',
        description: 'Описание верхнего раздела2',
        dateAdd: '2020/10/25',
        links: ['https://www.youtube.com/watch?v=zZBiln_2FhM&t=20s'],
        submaterial: []
      }, {
        uuid: 1233212,
        parentUuid: 123321,
        title: 'Средний раздел',
        description: 'Описание среднего раздела',
        dateAdd: '2020/10/25',
        links: ['https://www.youtube.com/watch?v=zZBiln_2FhM&t=20s'],
        submaterial: []
      }, {
        uuid: 1233214,
        parentUuid: 123321,
        title: 'Средний раздел2',
        description: 'Описание среднего раздела2',
        dateAdd: '2020/10/25',
        links: ['https://www.youtube.com/watch?v=zZBiln_2FhM&t=20s'],
        submaterial: []
      }, {
        uuid: 1233213,
        parentUuid: 1233212,
        title: 'Нижний раздел',
        description: 'Описание нижнего раздела',
        dateAdd: '2020/10/25',
        links: ['https://www.youtube.com/watch?v=zZBiln_2FhM&t=20s'],
        submaterial: []
      }, {
        uuid: 1233218,
        parentUuid: null,
        title: 'Потеряный раздел',
        description: 'Описание потеряного раздела',
        dateAdd: '2020/10/25',
        links: ['https://www.youtube.com/watch?v=zZBiln_2FhM&t=20s'],
        submaterial: []
      }]
    };
  },
  getters: {
    getBase: state => state.knowledgeBase,

    getRoot: state => {
      let root = {uuid: 0, parentUuid: null, submaterial: []};
      let materialList = {0: root};

      for (let item of state.knowledgeBase) {
        let uuid, parentUuid;
        item.uuid === null || item.uuid === undefined ? uuid = 0 : uuid = item.uuid;
        item.parentUuid === null || item.parentUuid === undefined ? parentUuid = 0 : parentUuid = item.parentUuid;

        materialList[uuid] = JSON.parse(JSON.stringify(item));
        materialList[parentUuid].submaterial.push(materialList[uuid]);
      }
      // console.table (materialList)
      return [root]
    }
  },
  mutations: {
    addMaterial: (state, payload) => {
      state.knowledgeBase.push(JSON.parse(JSON.stringify(payload)));
    }
  },
  actions: {
    addMaterial(context, payload) {
      context.commit("addMaterial", payload)

    }
  }
};
