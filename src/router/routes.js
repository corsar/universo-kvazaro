const routes = [
  {
    path: "/",
    component: () => import("layouts/StubMainLayout.vue"),
    children: [{ path: "", component: () => import("pages/StubIndex.vue") }]
  },
  {
    path: "/test",
    component: () => import("layouts/MainLayout.vue"),
    children: [
      { path: "/test", component: () => import("pages/Index.vue") },
      { path: "/test/gk", component: () => import("pages/komunumoj.vue") },
      { path: "/test/gu", component: () => import("pages/uzantoj.vue") },
      { path: "/test/kbase", component: () => import("pages/knowledgeBase.vue") },
      { path: "/test/registrado", component: () => import("pages/registrado") },
      { path: "/test/u:id", component: () => import("pages/uzanto") },
      { path: "/test/p:uuid", component: () => import("pages/projekto") },
      { path: "/test/k:id", component: () => import("pages/komunumo") }
    ]
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: "*",
    component: () => import("pages/Error404.vue")
  }
];

export default routes;
