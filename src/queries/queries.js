import gql from "graphql-tag";
//запрос для получения списка пользователей
export const uzantojQuery = gql`
  query Uzantoj($first: Int = 1, $after: String, $serchi: String) {
    lasto: uzantoj(last: 1) {
      pageInfo {
        hasPreviousPage
        startCursor
        endCursor
      }
      edges {
        node {
          uuid
          objId
          id
        }
      }
    }
    uzantoj(
      first: $first
      after: $after
      isActive: true
      konfirmita: true
      serchi: $serchi
      orderBy: ["-krea_dato"]
    ) {
      pageInfo {
        hasNextPage
        startCursor
        endCursor
      }
      edges {
        node {
          id
          objId
          uuid
          unuaNomo {
            enhavo
          }
          duaNomo {
            enhavo
          }
          familinomo {
            enhavo
          }
          sekso
          statistiko {
            miaGekamarado
            miaGekamaradoPeto
            kandidatoGekamarado
            tutaGekamaradoj
            rating
            aktivaDato
          }
          isActive
          konfirmita
          avataro {
            bildoE {
              url
            }
            bildoF {
              url
            }
          }
          statuso {
            enhavo
          }
          kontaktaInformo
        }
      }
    }
  }
`;
//запрос для получения данных пользователя по objID
export const uzantoByObjIdQuery = gql`
  query Uzanto($id: Float!) {
    uzanto: uzantoj(objId: $id) {
      edges {
        node {
          id
          objId
          uuid
          unuaNomo {
            enhavo
          }
          duaNomo {
            enhavo
          }
          familinomo {
            enhavo
          }
          sekso
          statistiko {
            miaGekamarado
            miaGekamaradoPeto
            kandidatoGekamarado
            tutaGekamaradoj
            rating
            aktivaDato
          }
          avataro {
            bildoE {
              url
            }
            bildoF {
              url
            }
          }
          statuso {
            enhavo
          }
          kontaktaInformo
        }
      }
    }
  }
`;
//запрос для получения списка товарищей
export const gekamaradojQuery = gql`
  query Gekamaradoj($first: Int = 1, $after: String, $serchi: String) {
    lasto: gekamaradoj(last: 1) {
      pageInfo {
        hasPreviousPage
        startCursor
        endCursor
      }
      edges {
        node {
          uuid
          objId
          id
        }
      }
    }
    gekamaradoj(
      first: $first
      after: $after
      isActive: true
      konfirmita: true
      serchi: $serchi
      orderBy: ["-krea_dato"]
    ) {
      pageInfo {
        hasNextPage
        startCursor
        endCursor
      }
      edges {
        node {
          id
          objId
          uuid
          unuaNomo {
            enhavo
          }
          duaNomo {
            enhavo
          }
          familinomo {
            enhavo
          }
          sekso
          statistiko {
            miaGekamarado
            miaGekamaradoPeto
            kandidatoGekamarado
            tutaGekamaradoj
            rating
            aktivaDato
          }
          isActive
          konfirmita
          avataro {
            bildoE {
              url
            }
            bildoF {
              url
            }
          }
          statuso {
            enhavo
          }
          kontaktaInformo
        }
      }
    }
  }
`;
// Запрос для получения списка всех сообществ
export const komunumojQuery = gql`
  query Komunumoj(
    $first: Int = 1
    $after: String
    $enDato: DateTime
    $serchi: String
    $tipo: String
  ) {
    lasto: komunumoj(last: 1, tipo_Kodo: $tipo) {
      pageInfo {
        hasPreviousPage
        startCursor
        endCursor
      }
      edges {
        node {
          uuid
          objId
          id
        }
      }
    }
    komunumoj(
      first: $first
      after: $after
      enDato: $enDato
      serchi: $serchi
      tipo_Kodo_In: $tipo
      orderBy: ["-rating", "-aktiva_dato"]
    ) {
      pageInfo {
        hasNextPage
        startCursor
        endCursor
      }
      edges {
        node {
          id
          objId
          uuid
          nomo {
            enhavo
          }
          priskribo {
            enhavo
          }
          statistiko {
            postulita
            tuta
            mia
            membraTipo
          }
          avataro {
            bildoE {
              url
            }
            bildoF {
              url
            }
          }
          kovrilo {
            bildoE {
              url
            }
          }
          statistiko {
            tuta
          }
        }
      }
    }
  }
`;
// Запрос для получения данных конкретного сообщества по objID
export const komunumoQuery = gql`
  query Komunumo($id: Float = 13) {
    komunumo: komunumoj(objId: $id) {
      edges {
        node {
          id
          objId
          uuid
          tipo {
            kodo
            nomo {
              enhavo
            }
          }
          nomo {
            enhavo
          }
          priskribo {
            enhavo
          }
          statistiko {
            postulita
            tuta
            mia
            membraTipo
          }
          avataro {
            bildoE {
              url
            }
            bildoF {
              url
            }
          }
          kovrilo {
            bildoE {
              url
            }
          }
          informo {
            enhavo
          }
          informoBildo {
            bildo
            bildoMaks
          }
          kontaktuloj {
            edges {
              node {
                objId
                unuaNomo {
                  enhavo
                }
                duaNomo {
                  enhavo
                }
                familinomo {
                  enhavo
                }
                avataro {
                  bildoF {
                    url
                  }
                }
                kontaktaInformo
              }
            }
          }
          rajtoj
        }
      }
    }
  }
`;
//запрос для получения списка проектов сообщества по ID сообщества
//ВАЖНО!!! Пока используется схема universoTaskojProjekto так как после разделения universoProjektojProjekto пустая
export const projektojQuery = gql`
  query Projektoj($komunumoId: Float, $first: Int = 25) {
    projektoj: universoTaskojProjekto(
      first: $first
      universotaskojprojektoposedanto_PosedantoKomunumo_Id: $komunumoId
    ) {
      edges {
        node {
          uuid
          nomo {
            enhavo
            __typename
          }
          priskribo {
            enhavo
            __typename
          }
          tipo {
            nomo {
              enhavo
              __typename
            }
            __typename
          }
          pozicio
          realeco {
            nomo {
              enhavo
              __typename
            }
            __typename
          }
          kategorio {
            edges {
              node {
                nomo {
                  enhavo
                  __typename
                }
                __typename
              }
              __typename
            }
            __typename
          }
          statuso {
            nomo {
              enhavo
              __typename
            }
            __typename
          }
          objekto {
            nomo {
              enhavo
              __typename
            }
            __typename
          }
          universotaskojprojektoposedantoSet {
            edges {
              node {
                projekto {
                  nomo {
                    enhavo
                    __typename
                  }
                  __typename
                }
                __typename
              }
              __typename
            }
            __typename
          }
          tasko {
            edges {
              node {
                id
                nomo {
                  enhavo
                  __typename
                }
                priskribo {
                  enhavo
                  __typename
                }
                __typename
              }
              __typename
            }
            __typename
          }
          __typename
        }
        __typename
      }
      __typename
    }
  }
`;
//запрос для получения данных проекта по UUID. После реализации в бакенде поменяем на ID
//ВАЖНО!!! Пока используется схема universoTaskojProjekto так как после разделения universoProjektojProjekto пустая
export const projektoByUuidQuery = gql`
  query Projekto($uuid: UUID) {
    projekto: universoTaskojProjekto(uuid: $uuid) {
      edges {
        node {
          uuid
          nomo {
            enhavo
          }
          priskribo {
            enhavo
          }
          tipo {
            nomo {
              enhavo
            }
          }
          pozicio
          realeco {
            nomo {
              enhavo
            }
          }
          kategorio {
            edges {
              node {
                nomo {
                  enhavo
                }
              }
            }
          }
          statuso {
            nomo {
              enhavo
            }
          }
          objekto {
            nomo {
              enhavo
            }
          }
          universotaskojprojektoposedantoSet {
            edges {
              node {
                projekto {
                  nomo {
                    enhavo
                  }
                }
              }
            }
          }
          tasko {
            edges {
              node {
                id
                nomo {
                  enhavo
                }
                priskribo {
                  enhavo
                }
              }
            }
          }
        }
      }
    }
  }
`;
//типы сообществ
export const komunumojTipojQuery = gql`
  query komunumojTipoy {
    komunumojTipoj {
      edges {
        node {
          nomo {
            enhavo
          }
          kodo
          kvantoKomomunumoj
        }
      }
    }
  }
`;
