import Vue from 'vue';
import axios from 'axios';

const axiosInstance = axios.create({
  baseURL: process.env.API_URL || 'http://localhost:8000/api/v1.1/',
  timeout: 10000,
  withCredentials: true,
  xsrfHeaderName: 'X-CSRFTOKEN',
  xsrfCookieName: 'csrftoken'
})

// axiosInstance.defaults.headers.common['Access-Control-Allow-Origin'] = '*'

Vue.prototype.$axios = axiosInstance
export { axiosInstance }
